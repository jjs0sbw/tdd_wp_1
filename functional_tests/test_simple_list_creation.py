from .base import FunctionalTest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

## Functional Tests


class NewVisitorTest(FunctionalTest):

    def test_can_start_a_list_and_retrieve_it_later(self):
        # Pete has heard about a cool new online to-do app.
        # He checks out its homepage
        self.browser.get(self.server_url)

        # He observes that the page header and title mention to-do lists
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # He is invited to enter a to-do item immediatly 
        inputbox = self.get_item_input_box()
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )

        # He types "Buy organic flour" into the text box (Pete's hobby
        # is baking artisian breads)
        inputbox.send_keys('Buy organic flour.')
        

        # When he hits enter, he is taken to a new URL,
        # and now the page lists "1: Buy organic flour" as an item in a 
        #to-do list table
        inputbox.send_keys(Keys.ENTER)
        pete_list_url = self.browser.current_url
        #self.assertRegex(pete_list_url, '/lists/.+')
        self.assertRegex(pete_list_url, '/lists/.+')
        self.check_for_row_in_list_table('1: Buy organic flour.')

        #table = self.browser.find_element_by_id('id_list_table')
        #rows = table.find_elements_by_tag_name('tr')
        #self.assertIn('1: Buy organic flour', [row.text for row in rows])
        


        # There is still a text box inviting him to add another list item.
        # Pete enters, "Bake bread with the orgainc flour."
        inputbox = self.get_item_input_box()
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )
        inputbox.send_keys('Bake bread with the organic flour.')
        inputbox.send_keys(Keys.ENTER)
        pete_list_url = self.browser.current_url
        self.assertRegex(pete_list_url, '/lists/.+')

        # The page updates again, and now shows both items on his list.
        #self.check_for_row_in_list_table('2: Bake bread with the organic flour.')
        self.check_for_row_in_list_table('1: Buy organic flour.')
        self.check_for_row_in_list_table('2: Bake bread with the organic flour.')
        

        #dtable = self.browser.find_element_by_id('id_list_table')
        #drows = table.find_elements_by_tag_name('tr')
        #dself.assertIn('1: Buy organic flour', [row.text for row in rows])
        #dself.assertIn(
        #d    '2: Bake bread with the organic flour.',
        #d    [row.text for row in rows]
        #d)

        #d Pete wonders whether the site will remember his list.
        #d He notices that the site has generated a unique URL for his list
        #d There is some explanatory text to that effect.
        
        # Now a new user Jesse, comes along to the site.
        self.browser.quit()

        # A new browser session is used to make sure that no information 
        # from Pete's session is coming through from cookies, etc.
        self.browser = webdriver.Firefox()

        # Jesse visits the home page. There is no sign of Pete's list
        self.browser.get(self.server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy organic flour.', page_text)
        self.assertNotIn('Bake bread with', page_text)

        # Jesse starts a new list by entering a new item.  She
        # is less interesting than Pete
        inputbox = self.get_item_input_box()
        inputbox.send_keys('Buy cheese')
        inputbox.send_keys(Keys.ENTER)

        # Jesse gets her own unique URL
        jesse_list_url = self.browser.current_url
        self.assertRegex(jesse_list_url, '/lists/.+')
        self.assertNotEqual(pete_list_url, jesse_list_url)

        # There is no sign of Pete's list items
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy organic flour.', page_text)
        self.assertIn('Buy cheese', page_text)

        # Satisified, he goes for a long run

        # Pete visits the URL link and finds his to-do list.

        # Impressed with the list, Pete goes to the pub for a beer.
 

        

