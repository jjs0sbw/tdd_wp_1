from unittest import skip
from .base import FunctionalTest


class ItemValidationTest(FunctionalTest):

    def get_error_element(self):
        return self.browser.find_element_by_css_selector('.has-error')


    def test_cannot_add_empty_list_items(self):
        # Pete goes to the home page and accidentally tries to submit
        # an empty list item.  He hits ENTER on the empty input box.
        self.browser.get(self.server_url)
        self.get_item_input_box().send_keys('\n')

        # The home page refreshes, and there is and error message 
        # saying that the list items cannot be blank
        error = self.get_error_element()
        self.assertEqual(error.text, "You can not have an empty list item.")

        # He tries again with some text and now it works
        self.browser.get(self.server_url)
        self.get_item_input_box().send_keys('Make some butter\n')

        # He tries to enter a second blank list item
        self.get_item_input_box().send_keys('\n') 

        # The error messge is preseneted to him again
        self.check_for_row_in_list_table('1: Make some butter')
        error = self.get_error_element()
        self.assertEqual(error.text, "You can not have an empty list item.")

        # The error is corrected by filling in some text
        self.get_item_input_box().send_keys('Make jam\n')
        self.check_for_row_in_list_table('1: Make some butter')
        self.check_for_row_in_list_table('2: Make jam')

        # Pete goes to the home page and starts a new list
        self.browser.get(self.server_url)
        self.get_item_input_box().send_keys('Write a book\n')
        self.check_for_row_in_list_table('1: Write a book')

        # He accidentally tries to enter a duplicate item
        self.get_item_input_box().send_keys('Write a book\n')
        
        # He sees a helpful error message
        self.check_for_row_in_list_table('1: Write a book')
        error = self.get_error_element()
        self.assertEqual(error.text, "You have this item in the list.")

    def test_error_messages_are_cleared_on_input(self):
        # Pete starts a new list in a way that causes a validation error:
        self.browser.get(self.server_url)
        self.get_item_input_box().send_keys('\n')
        error = self.get_error_element()
        self.assertTrue(error.is_displayed())

        # He starts typing in the input box to clear the error
        self.get_item_input_box().send_keys('a')

        # He is pleased to see that the error message disappears
        error = self.get_error_element()
        self.assertFalse(error.is_displayed())























        self.fail('Finish the test!!')
        
