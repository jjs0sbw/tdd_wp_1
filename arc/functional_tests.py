from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        # Pete has heard about a cool new online to-do app.
        # He checks out its homepage
        self.browser.get('http://localhost:8000')

        # He observes that the page header and title mention to-do lists
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # He is invited to enter a to-do item immediatly 
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )

        # He types "Buy organic flour" into the text box (Pete's hobby
        # is baking artisian breads)
        inputbox.send_keys('Buy organic flour.')
        
        # When he hits enter, the page updates, and now the page lists
        # "1: Buy organic flour" as an item in a to-do list
        inputbox.send_keys(Keys.ENTER)
        self.check_for_row_in_list_table('1: Buy organic flour.')

        #table = self.browser.find_element_by_id('id_list_table')
        #rows = table.find_elements_by_tag_name('tr')
        #self.assertIn('1: Buy organic flour', [row.text for row in rows])
        


        # There is still a text box inviting him to add another list item.
        # Pete enters, "Bake bread with the orgainc flour."
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Bake bread with the organic flour.')
        inputbox.send_keys(Keys.ENTER)

        # The page updates again, and now shows both items on her list.
        self.check_for_row_in_list_table('1: Buy organic flour.')
        self.check_for_row_in_list_table('2: Bake bread with the organic flour.')
        

        #table = self.browser.find_element_by_id('id_list_table')
        #rows = table.find_elements_by_tag_name('tr')
        #self.assertIn('1: Buy organic flour', [row.text for row in rows])
        #self.assertIn(
        #    '2: Bake bread with the organic flour.',
        #    [row.text for row in rows]
        #)

        # Pete wonders whether the site will remember his list.
        # He notices that the site has generated a unique URL for his list
        # There is some explanatory text to that effect.
        self.fail('Finish the test!!')
        # Pete visits the URL link and finds his to-do list.

        # Impressed with the list, Pete goes to the pub for a beer.

if __name__ == '__main__':
    unittest.main(warnings='ignore')

