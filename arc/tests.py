from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest
from unittest import skip
import sys
## Functional Tests

class FunctionalTest(LiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        for arg in sys.argv:
            if 'liveserver' in arg:
                cls.server_url = 'http://' + arg.split('=')[1]
                return
        LiveServerTestCase.setUpClass()
        cls.server_url = cls.live_server_url

    @classmethod
    def tearDownClass(cls):
        if cls.server_url == cls.live_server_url:
            LiveServerTestCase.tearDownClass()

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])


class LayoutAndStylingTest(FunctionalTest):
   
    def test_layout_and_styling(self):
        #Pete goes to the home page
        self.browser.get(self.server_url)
        self.browser.set_window_size(1024, 768)

        #He notices the input box is nicely centered
        inputbox = self.browser.find_element_by_tag_name('input')
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width']/2,
            512,
            delta=3
        )

        #He starts a new list and sees the inout in 
        #nicely centered there also
        inputbox.send_keys('testing\n')
        inputbox = self.browser.find_element_by_tag_name('input')
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width']/2,
            512,
            delta=3
        )
class NewVisitorTest(FunctionalTest):

    def test_can_start_a_list_and_retrieve_it_later(self):
        # Pete has heard about a cool new online to-do app.
        # He checks out its homepage
        self.browser.get(self.server_url)

        # He observes that the page header and title mention to-do lists
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # He is invited to enter a to-do item immediatly 
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )

        # He types "Buy organic flour" into the text box (Pete's hobby
        # is baking artisian breads)
        inputbox.send_keys('Buy organic flour.')
        

        # When he hits enter, he is taken to a new URL,
        # and now the page lists "1: Buy organic flour" as an item in a 
        #to-do list table
        inputbox.send_keys(Keys.ENTER)
        pete_list_url = self.browser.current_url
        #self.assertRegex(pete_list_url, '/lists/.+')
        self.assertRegex(pete_list_url, '/lists/.+')
        self.check_for_row_in_list_table('1: Buy organic flour.')

        #table = self.browser.find_element_by_id('id_list_table')
        #rows = table.find_elements_by_tag_name('tr')
        #self.assertIn('1: Buy organic flour', [row.text for row in rows])
        


        # There is still a text box inviting him to add another list item.
        # Pete enters, "Bake bread with the orgainc flour."
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )
        inputbox.send_keys('Bake bread with the organic flour.')
        inputbox.send_keys(Keys.ENTER)
        pete_list_url = self.browser.current_url
        self.assertRegex(pete_list_url, '/lists/.+')

        # The page updates again, and now shows both items on his list.
        #self.check_for_row_in_list_table('2: Bake bread with the organic flour.')
        self.check_for_row_in_list_table('1: Buy organic flour.')
        self.check_for_row_in_list_table('2: Bake bread with the organic flour.')
        

        #dtable = self.browser.find_element_by_id('id_list_table')
        #drows = table.find_elements_by_tag_name('tr')
        #dself.assertIn('1: Buy organic flour', [row.text for row in rows])
        #dself.assertIn(
        #d    '2: Bake bread with the organic flour.',
        #d    [row.text for row in rows]
        #d)

        #d Pete wonders whether the site will remember his list.
        #d He notices that the site has generated a unique URL for his list
        #d There is some explanatory text to that effect.
        
        # Now a new user Jesse, comes along to the site.
        self.browser.quit()

        # A new browser session is used to make sure that no information 
        # from Pete's session is coming through from cookies, etc.
        self.browser = webdriver.Firefox()

        # Jesse visits the home page. There is no sign of Pete's list
        self.browser.get(self.server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy organic flour.', page_text)
        self.assertNotIn('Bake bread with', page_text)

        # Jesse starts a new list by entering a new item.  She
        # is less interesting than Pete
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Buy cheese')
        inputbox.send_keys(Keys.ENTER)

        # Jesse gets her own unique URL
        jesse_list_url = self.browser.current_url
        self.assertRegex(jesse_list_url, '/lists/.+')
        self.assertNotEqual(pete_list_url, jesse_list_url)

        # There is no sign of Pete's list items
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy organic flour.', page_text)
        self.assertIn('Buy cheese', page_text)

        # Satisified, he goes for a long run

        # Pete visits the URL link and finds his to-do list.

        # Impressed with the list, Pete goes to the pub for a beer.
 
class ItemValidationTest(FunctionalTest):

    @skip
    def test_cannot_add_empty_list_items(self):
        #Pete goes to the home page and accidentally tries to submit
        # an empty list item.  He hits ENTER on the empty input box.

        # The home page refreshes, and there is and error message 
        # saying that the list items cannot be blank

        # He tries agian with some text and noe it works

        # He tries to enter a second blank list item

        # The error messge is preseneted to him again

        # The error is corrected by filling in some text

        self.fail('Finish the test!!')
        




#if __name__ == '__main__':
#    unittest.main(warnings='ignore')

